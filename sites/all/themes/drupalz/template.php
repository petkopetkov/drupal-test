<?php
/*
* Override breadcrumbs for taxonomy page
*/
function drupalz_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  if ( arg(1) == 'term') {
    $home = $breadcrumb[0];
    unset($breadcrumb);
    $events_view = views_get_view('location_table');
    $term = taxonomy_term_load(arg(2));
    $breadcrumb[] = $home;
    $breadcrumb[] = l('Events',$events_view->display['page']->display_options['path']);
    $breadcrumb[] = $term->name;
  }
  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode(' » ', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Override or insert variables into the page template.
 */
function drupalz_preprocess_page(&$variables) {


if(isset($variables['node']->type) && $variables['node']->type == 'article'){


  $author_id = $variables['node']->uid;
  $cu_id = $variables['user']->uid;

  $cu_img = drupalz_get_user_picture($cu_id);
  $author_img = drupalz_get_user_picture($author_id);


  if(empty($cu_img) && $cu_id != 1){
    dpm($cu_img);
     unset($variables['action_links'][0]);
  }

  if(!empty($author_img)){

    $img_url = '<img src="'. image_style_url('thumbnail', $author_img).'"/>';
    $variables['title_suffix'] = $img_url;

  }

}


// set title to term page

  if ( arg(1) == 'term') {
      $term = taxonomy_term_load(arg(2));
      drupal_set_title($term->name);
      $variables['title'] = $term->name;

  }

}

// get avatar from uid

function drupalz_get_user_picture($uid){

    $user_field = user_load($uid);

   return $user_field->picture ? $user_field->picture->uri : false;

}
